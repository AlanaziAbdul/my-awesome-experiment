import time
import random
import matplotlib.pyplot as plt


#source for merge sort is from GeeksforGeeks https://www.geeksforgeeks.org/python-program-for-merge-sort/
def merge(arr, l, m, r):
    n1 = m - l + 1
    n2 = r - m
 
    # create temp arrays
    L = [0] * (n1)
    R = [0] * (n2)
 
    # Copy data to temp arrays L[] and R[]
    for i in range(0, n1):
        L[i] = arr[l + i]
 
    for j in range(0, n2):
        R[j] = arr[m + 1 + j]
 
    # Merge the temp arrays back into arr[l..r]
    i = 0     # Initial index of first subarray
    j = 0     # Initial index of second subarray
    k = l     # Initial index of merged subarray
 
    while i < n1 and j < n2:
        if L[i] <= R[j]:
            arr[k] = L[i]
            i += 1
        else:
            arr[k] = R[j]
            j += 1
        k += 1
 
    # Copy the remaining elements of L[], if there
    # are any
    while i < n1:
        arr[k] = L[i]
        i += 1
        k += 1
 
    # Copy the remaining elements of R[], if there
    # are any
    while j < n2:
        arr[k] = R[j]
        j += 1
        k += 1
 
# l is for left index and r is right index of the
# sub-array of arr to be sorted
 
 
def mergeSort(arr, l, r):
    if l < r:
 
        # Same as (l+r)//2, but avoids overflow for
        # large l and h
        m = l+(r-l)//2
 
        # Sort first and second halves
        mergeSort(arr, l, m)
        mergeSort(arr, m+1, r)
        merge(arr, l, m, r)

# This code for merge function is contributed by Mohit Kumra




# Insertion Sort Implementation, Source is GeeksforGeeks = https://www.geeksforgeeks.org/python-program-for-insertion-sort/   
def insertion_sort(arr):
    n = len(arr)  # Get the length of the array
      
    if n <= 1:
        return  # If the array has 0 or 1 element, it is already sorted, so return
 
    for i in range(1, n):  # Iterate over the array starting from the second element
        key = arr[i]  # Store the current element as the key to be inserted in the right position
        j = i-1
        while j >= 0 and key < arr[j]:  # Move elements greater than key one position ahead
            arr[j+1] = arr[j]  # Shift elements to the right
            j -= 1
        arr[j+1] = key  # Insert the key in the correct position




# Function to compare the sorting algorithms
def compare_sorts(input_sizes):
    merge_times = []
    insertion_times = []

    for size in input_sizes:
        arr = [random.randint(1, 10000) for _ in range(size)]


        
        # time for merge sort function
        start_time = time.time()
        mergeSort(arr.copy(), 0, size - 1)
        end_time = time.time()
        merge_times.append(end_time - start_time)


        
        # time for insertion sort function
        start_time = time.time()
        insertion_sort(arr.copy())
        end_time = time.time()
        insertion_times.append(end_time - start_time)

    return merge_times, insertion_times



input_sizes = [1, 10,20,30,40,50,60,70,80,90,100,110,120,130,140]
merge_times, insertion_times = compare_sorts(input_sizes)
plt.plot(input_sizes, merge_times, label='Merge Sort')
plt.plot(input_sizes, insertion_times, label='Insertion Sort')
plt.xlabel('Input Size')
plt.ylabel('Time (seconds)')
plt.title('Merge Sort vs Insertion Sort Time Comparison')
plt.legend()
plt.show()


